<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\Process\Process;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->namespace('api')->group(function () {
    Route::post('files','FileController@createFile');
    Route::get('files','FileController@getFiles');
    Route::post('directories','FileController@createDirectories');
    Route::get('directories','FileController@getDirectories');
    Route::get('processes','ProcessController@getProcesses');
});

