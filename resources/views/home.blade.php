@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <!-- Process -->
                    <div class="row justify-content-center mt-12">
                        <div class="col-md-12">
                            <process-component></process-component>
                        </div>
                    </div>
                    <!-- Process End -->
                    <hr>
                    <!-- File -->
                    <div class="row justify-content-center mt-12">
                        <div class="col-md-12">
                            <file-component></file-component>
                        </div>
                    </div>
                    <!-- File End -->
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
