<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Traits\execCommand;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProcessController extends Controller
{
    use execCommand;
    public function getProcesses(Request $request)
    {
        $cmd = 'ps -aux';
        $processes = $this->execCommand($cmd);
        return response()->json(['processes' => $processes])->setStatusCode(Response::HTTP_OK);
    }

    private function execCommand($cmd)
    {
        $pattern = '/'; // Start
        $pattern .= '([\S]+)'; // USER - 1
        $pattern .= '[\s]+';
        $pattern .= '([\S]+)'; // PID - 2
        $pattern .= '[\s]+';
        $pattern .= '([\S]+)'; // %CPU - 3
        $pattern .= '[\s]+';
        $pattern .= '([\S]+)'; // %MEM - 4
        $pattern .= '[\s]+';
        $pattern .= '([\S]+)'; // VSZ - 5
        $pattern .= '[\s]+';
        $pattern .= '([\S]+)'; // RSS - 6
        $pattern .= '[\s]+';
        $pattern .= '([\S]+)'; // TTY - 7
        $pattern .= '[\s]+';
        $pattern .= '([\S]+)'; // STAT - 8
        $pattern .= '[\s]+';
        $pattern .= '([\S]+)'; // START - 9
        $pattern .= '[\s]+';
        $pattern .= '([\S]+)'; // TIME - 10
        $pattern .= '[\s]+';
        $pattern .= '([\S\s]+)'; // COMMAND - 11
        $pattern .= '/'; // End

        $processes = [];
        $output = $this->execute($cmd);
        unset($output[0]);
        foreach ($output as $index => $process) {
            preg_match($pattern, $process, $matches);
            $ps['USER'] = $matches[1];
            $ps['PID'] = $matches[2];
            $ps['CPU'] = $matches[3];
            $ps['MEM'] = $matches[4];
            $ps['VSZ'] = $matches[5];
            $ps['RSS'] = $matches[6];
            $ps['TTY'] = $matches[7];
            $ps['STAT'] = $matches[8];
            $ps['START'] = $matches[9];
            $ps['TIME'] = $matches[10];
            $ps['COMMAND'] = $matches[11];
            array_push($processes, $ps);
        }
        return $processes;
    }

}
