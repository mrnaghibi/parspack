<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Traits\execCommand;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class FileController extends Controller
{
    use execCommand;

    public function createFile(Request $request)
    {
        $path = config('setting.DIST_DIR');
        $cmd = "touch {$path}{$request->user()->name}.txt";
        $this->execute($cmd);
        return response()->json(['message' => "File Created Successfully!"])->setStatusCode(Response::HTTP_OK);
    }
    public function createDirectories(Request $request)
    {
        $path = config('setting.DIST_DIR');
        $cmd = "mkdir {$path}{$request->user()->name}";
        $this->execute($cmd);
        return response()->json(['message' => "Directory Created Successfully!"])->setStatusCode(Response::HTTP_OK);
    }
    public function getFiles(Request $request)
    {
        $path = config('setting.DIST_DIR');
        $cmd = "ls -al {$path} | grep '^-'";
        $files = $this->execCommand($cmd);
        return response()->json(['files' => $files])->setStatusCode(Response::HTTP_OK);
    }
    public function getDirectories(Request $request)
    {
        $path = config('setting.DIST_DIR');
        $cmd = "ls -al {$path} | grep '^d'";
        $directories = $this->execCommand($cmd);
        return response()->json(['directories' => $directories])->setStatusCode(Response::HTTP_OK);
    }

    private function execCommand($cmd)
    {
        $pattern = '/';         // Start
        $pattern .= '([\S]+)'; // PERMISSION - 1
        $pattern .= '[\s]+';
        $pattern .= '([\S]+)'; // Number of hard links to the file - 2
        $pattern .= '[\s]+';
        $pattern .= '([\S]+)'; // USER - 3
        $pattern .= '[\s]+';
        $pattern .= '([\S]+)'; // GROUP - 4
        $pattern .= '[\s]+';
        $pattern .= '([\S]+)'; // SIZE - 5
        $pattern .= '[\s]+';
        $pattern .= '([\S]+)'; // MONTH - 6
        $pattern .= '[\s]+';
        $pattern .= '([\S]+)'; // DAY - 7
        $pattern .= '[\s]+';
        $pattern .= '([\S]+)'; // TIME - 8
        $pattern .= '[\s]+';
        $pattern .= '([\S\s]+)';  // NAME - 9
        $pattern .= '/'; // End

        $files = [];
        $output = $this->execute($cmd);

        foreach ($output as $index => $file) {
            preg_match($pattern, $file, $matches);
            $ps['PERMISSION'] = $matches[1];
            $ps['OWNER'] = $matches[3];
            $ps['GROUP'] = $matches[4];
            $ps['SIZE'] = $matches[5];
            $ps['DATE'] = "$matches[6] - $matches[7] - $matches[8]";
            $ps['NAME'] = $matches[9];
            array_push($files, $ps);
        }
        return $files;
    }
}
