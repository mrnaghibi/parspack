<?php


namespace App\Traits;


trait execCommand
{
    public function execute($cmd)
    {
        exec($cmd, $output);
        return $output;
    }
}
